.model small
.code      
org 100h

;fungsi mencetak string
mov ah,09h
lea dx,kal0             ;masukkan nama
int 21h

;fungsi menginput string (disimpan di buffer)
mov ah,0ah
lea dx,kata1            ;input nama
int 21h         
                        
;fungsi mencetak string                        
mov ah,09h
lea dx,kal2             ;masukkan nim
int 21h

;fungsi menginput string (disimpan di buffer)
mov ah,0ah
lea dx,kata2            ;input nim
int 21h

;fungsi mencetak string 
mov ah,09h
lea dx,endl             ;endl
int 21h

;fungsi mencetak string
mov ah,09h
lea dx,kal1             ;nama : 
int 21h

;sintaks untuk mencetak string yang tadi diinput
xor bx,bx
mov bl,kata1[1]
mov kata1[bx+2], "$"
lea dx,kata1+2          ;cetak input nama
mov ah,09h
int 21h

;fungsi mencetak string
mov ah,09h
lea dx,kal3             ;nim :
int 21h      

;sintaks untuk mencetak string yang tadi diinput
xor bx,bx
mov bl,kata2[1]
mov kata2[bx+2], "$"
lea dx,kata2+2          ;cetak input nim
mov ah,09h
int 21h

;intrupsi untuk menghentika program
int 20h            
                
;deklarasi variable
kal0 db 13,10, "Masukkan nama : $"
kal1 db 13,10, "Nama : $"
kal2 db 13,10, "Masukkan NIM  : $"
kal3 db 13,10, "NIM  : $"
kata1 db 40,?,40 dup (' ')      
kata2 db 40,?,40 dup (' ')
endl db 0ah, 0dh, "$"
